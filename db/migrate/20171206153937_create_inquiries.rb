class CreateInquiries < ActiveRecord::Migration
  def change
    create_table :inquiries do |t|
      t.integer :user_id
      t.integer :kind_id
      t.string :body

      t.timestamps null: false
    end
  end
end

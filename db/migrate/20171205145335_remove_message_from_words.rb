class RemoveMessageFromWords < ActiveRecord::Migration
  def change
    remove_column :words, :message, :string, :limit => 300
  end
end

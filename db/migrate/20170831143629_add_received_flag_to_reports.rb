class AddReceivedFlagToReports < ActiveRecord::Migration
  def change
    add_column :reports, :received, :boolean, :null => false, :default => 0
  end
end

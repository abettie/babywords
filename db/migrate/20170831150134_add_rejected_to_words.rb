class AddRejectedToWords < ActiveRecord::Migration
  def change
    add_column :words, :rejected, :boolean, default: 0, null: false
  end
end

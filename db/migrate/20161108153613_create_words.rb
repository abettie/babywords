class CreateWords < ActiveRecord::Migration
  def change
    create_table :words do |t|
      t.string :word, :null => false, :limit => 40
      t.string :translate, :null => false, :limit => 100
      t.string :comment, :null => false, :limit => 300
      t.string :author_nickname, :null => false, :limit => 40
      t.integer :age_year, :null => false, :limit => 2
      t.integer :age_month, :null => false, :default => 99, :limit => 2
      t.string :gendar, :null => false, :default => 'm', :limit => 1
      t.string :message, :limit => 300
      t.boolean :adopted, :null => false, :default => 0

      t.timestamps :null => false
    end
  end
end

class RemoveAdoptedFromWords < ActiveRecord::Migration
  def change
    remove_column :words, :adopted, :boolean
  end
end

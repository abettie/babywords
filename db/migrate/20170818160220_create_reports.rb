class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.integer :word_id, :null => false
      t.integer :user_id, :null => true
      t.string :gip, :null => false, :limit => 15

      t.timestamps null: false
    end
  end
end

class RemoveNicknameFromWords < ActiveRecord::Migration
  def change
    remove_column :words, :author_nickname, :string
  end
end

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

def create_seeds (obj, datas)
	datas.each_with_index do |data, index| 
		obj.create(data) unless obj.exists?(:id => index+1)
	end
end

datas = [
	{name: '不具合報告'},
	{name: 'ご意見・ご要望'},
	{name: 'ご感想'},
	{id: 99, name: 'その他'},
]
create_seeds(InquiryKind, datas)
 
datas = [
	{provider: 'twitter', uid: '922129732879114240', sign_in_count: 1, current_sign_in_at: Time.now.to_s(:db), last_sign_in_at: Time.now.to_s(:db), current_sign_in_ip: '118.152.31.246', last_sign_in_ip: '118.152.31.246'},
	{provider: 'twitter', uid: '922131832606306304', sign_in_count: 1, current_sign_in_at: Time.now.to_s(:db), last_sign_in_at: Time.now.to_s(:db), current_sign_in_ip: '118.152.31.246', last_sign_in_ip: '118.152.31.246'},
]
create_seeds(User, datas)
 
datas = [
	{user_id: 1, nickname: '管理人'},
	{user_id: 2, nickname: 'てすとん'},
]
create_seeds(UserProfile, datas)

datas = [
	{word_id: 5, user_id: nil, gip: '10.128.93.1'},
	{word_id: 5, user_id: nil, gip: '10.128.93.2'},
	{word_id: 5, user_id: nil, gip: '10.128.93.3'},
	{word_id: 5, user_id: nil, gip: '10.128.93.4'},
	{word_id: 5, user_id: nil, gip: '10.128.93.5'},
	{word_id: 6, user_id: nil, gip: '10.128.93.1'},
	{word_id: 6, user_id: nil, gip: '10.128.93.2'},
	{word_id: 6, user_id: nil, gip: '10.128.93.3'},
	{word_id: 7, user_id: nil, gip: '10.128.93.1'},
	{word_id: 7, user_id: nil, gip: '10.128.93.2'},
	{word_id: 7, user_id: nil, gip: '10.128.93.3'},
	{word_id: 8, user_id: 1, gip: '10.128.93.2'},
]
create_seeds(Report, datas)

datas = [
	{user_id: 1, age_year: 2, age_month: 3, gendar: 'm', word: 'くしゅめたい', translate: 'くすぐったい', comment: ''},
	{user_id: 1, age_year: 2, age_month: 6, gendar: 'm', word: 'がお', translate: 'ライオン', comment: ''},
	{user_id: 1, age_year: 3, age_month: 9, gendar: 'm', word: 'おとうさんいまカチャカチャしてるの。', translate: 'お父さん今、仕事してるの。', comment: '私はSEをしており、たまに家で仕事をするので、いつの間にか息子の中で私の仕事は「カチャカチャ」になりました。'},
	{user_id: 1, age_year: 3, age_month: 9, gendar: 'm', word: 'しかぴーばす', translate: 'ズーラシアのオカピーバス', comment: ''},
	{user_id: 1, age_year: 3, age_month: 99, gendar: 'm', word: 'にうにう。', translate: '牛乳', comment: '「ぎ」が言えなかった。'},
	{user_id: 1, age_year: 3, age_month: 99, gendar: 'm', word: 'たいもす。', translate: 'タイガーモス', comment: '天空の城ラピュタに出てくるタイガーモスが発音できずに。。。'},
	{user_id: 1, age_year: 3, age_month: 99, gendar: 'm', word: 'テスト1', translate: 'テスト訳1', comment: 'comment 1'},
	{user_id: 1, age_year: 3, age_month: 99, gendar: 'm', word: 'テスト2', translate: 'テスト訳2', comment: 'comment 2'},
	{user_id: 1, age_year: 3, age_month: 99, gendar: 'm', word: 'テスト3', translate: 'テスト訳3', comment: 'comment 3'},
	{user_id: 1, age_year: 3, age_month: 99, gendar: 'm', word: 'たいもす。', translate: 'タイガーモス', comment: '天空の城ラピュタに出てくるタイガーモスが発音できずに。。。'},
	{user_id: 1, age_year: 3, age_month: 99, gendar: 'm', word: 'くるまにきをつけてねー。', translate: '車に気をつけてねー', comment: 'ヘリコプターに対して。'},
	{user_id: 1, age_year: 3, age_month: 99, gendar: 'm', word: 'おうちかえっちゃった。', translate: '家に帰ってしまった', comment: '救急車に対して。'},
	{user_id: 1, age_year: 4, age_month: 99, gendar: 'm', word: 'めんど。', translate: '粘土', comment: 'まさか「ね」が「め」になるとは。息子が何のことを言っているのかなかなか分かってあげられなかった。'},
	{user_id: 1, age_year: 4, age_month: 99, gendar: 'm', word: 'こうえんいってきなさい！', translate: '公園に行ってきなさい！', comment: '何か本人が気に入らないことがあると親に向かってプンプンしながら言ってきた（笑）'},
	{user_id: 1, age_year: 4, age_month: 99, gendar: 'm', word: 'かーぺりー', translate: 'カーフェリー', comment: '「フェ」は難しい。'},
	{user_id: 2, age_year: 5, age_month: 8, gendar: 'm', word: 'しつこくてうるせー！', translate: 'しつこくてうるさい。', comment: '2歳の妹が、車の中でグズっているのに耐えかねて。'},
	{user_id: 1, age_year: 5, age_month: 9, gendar: 'm', word: 'じゅっぴゃくえん', translate: '千円', comment: ''},
	{user_id: 2, age_year: 5, age_month: 9, gendar: 'm', word: 'はげつるぴっか', translate: 'ツルッパゲ', comment: '自分の赤ちゃんの頃の映像を見て一言。'},
	{user_id: 2, age_year: 5, age_month: 11, gendar: 'm', word: 'どうろでうたっていいの？', translate: '道路で歌ってもいいの？', comment: '安室奈美恵の新曲PVをCMで見て。'},
	{user_id: 1, age_year: 1, age_month: 7, gendar: 'f', word: 'ぶらぶらちょ。', translate: 'ブラブラしよ。', comment: '夜お休み前に、掛布団の上に寝かせてブラブラ揺らすのが恒例になっていて、その催促。'},
	{user_id: 1, age_year: 1, age_month: 9, gendar: 'f', word: 'ばーばーじょー', translate: 'ブラブラしよう。', comment: 'ぶらぶらちょの進化系。絵本読み終わった直後に発動する。'},
	{user_id: 1, age_year: 1, age_month: 10, gendar: 'f', word: 'らいらいらーい', translate: 'オーライオーライ', comment: '駐車でバックするときに。'},
	{user_id: 1, age_year: 1, age_month: 10, gendar: 'f', word: 'あまぉうよー。', translate: '読もうよー', comment: '絵本を一緒に読んでいるときに親がテレビに気を取られていると言われる。'},
	{user_id: 1, age_year: 1, age_month: 10, gendar: 'f', word: 'あぱいよー。', translate: 'おっぱい飲みたいよー', comment: '「お」が「あ」になりがちです。'},
	{user_id: 1, age_year: 1, age_month: 10, gendar: 'f', word: 'ごちゃーちゃーたー。', translate: 'ごちそうさまでした', comment: ''},
	{user_id: 1, age_year: 1, age_month: 11, gendar: 'f', word: 'てってい～い？', translate: 'これ使って遊んでいい？これ持っていっていい？', comment: 'この時期、何かにつけお伺いをたててました（笑）'},
	{user_id: 2, age_year: 2, age_month: 2, gendar: 'f', word: 'いったっちゃーす', translate: 'いただきます', comment: ''},
	{user_id: 2, age_year: 2, age_month: 2, gendar: 'f', word: 'ごっとーちた', translate: 'ごちそうさまでした', comment: ''},
	{user_id: 2, age_year: 2, age_month: 2, gendar: 'f', word: 'でえもん', translate: 'ドラえもん', comment: ''},
	{user_id: 2, age_year: 2, age_month: 2, gendar: 'f', word: 'だんぼーる', translate: 'ドラゴンボール', comment: ''},
	{user_id: 2, age_year: 2, age_month: 9, gendar: 'f', word: 'こっちにくるかーい', translate: 'こっちへおいで', comment: '誘うのではなく「問う」という新しい形。'},
	{user_id: 2, age_year: 2, age_month: 9, gendar: 'f', word: 'おっちゃま', translate: 'お月様', comment: ''},
	{user_id: 2, age_year: 2, age_month: 9, gendar: 'f', word: 'こーひーのぷりん', translate: 'コーヒーゼリー', comment: ''},
	{user_id: 2, age_year: 2, age_month: 10, gendar: 'f', word: 'ぱんかい', translate: '乾杯', comment: ''},
	{user_id: 2, age_year: 2, age_month: 11, gendar: 'f', word: 'きてくれたのー？', translate: '来てくれたの？', comment: '会社から帰ったら娘に言われました。。。愛人か！'},
	{user_id: 2, age_year: 5, age_month: 11, gendar: 'm', word: 'すぱどんずん', translate: 'スプラトゥーン2', comment: ''},
	{user_id: 2, age_year: 5, age_month: 3, gendar: 'f', word: 'TにてんてんつけるとD', translate: 'Tに点々付けるとD', comment: ''},
	{user_id: 2, age_year: 5, age_month: 4, gendar: 'f', word: 'すかべってぃ', translate: 'スパゲッティー', comment: ''},
]
create_seeds(Word, datas)

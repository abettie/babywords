# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171206153937) do

  create_table "inquiries", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "kind_id",    limit: 4
    t.string   "body",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "inquiry_kinds", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "reports", force: :cascade do |t|
    t.integer  "word_id",    limit: 4,                  null: false
    t.integer  "user_id",    limit: 4
    t.string   "gip",        limit: 15,                 null: false
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.boolean  "received",              default: false, null: false
  end

  create_table "user_profiles", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.string   "nickname",   limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "provider",            limit: 255,             null: false
    t.string   "uid",                 limit: 255,             null: false
    t.datetime "remember_created_at"
    t.string   "remember_token",      limit: 255
    t.integer  "sign_in_count",       limit: 4,   default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",  limit: 255
    t.string   "last_sign_in_ip",     limit: 255
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
  end

  add_index "users", ["provider", "uid"], name: "index_users_on_provider_and_uid", unique: true, using: :btree

  create_table "words", force: :cascade do |t|
    t.string   "word",       limit: 40,                  null: false
    t.string   "translate",  limit: 100,                 null: false
    t.string   "comment",    limit: 300,                 null: false
    t.integer  "age_year",   limit: 2,                   null: false
    t.integer  "age_month",  limit: 2,   default: 99,    null: false
    t.string   "gendar",     limit: 1,   default: "m",   null: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.integer  "user_id",    limit: 4
    t.boolean  "rejected",               default: false, null: false
  end

end

namespace :control_words do
  desc "Delete words because of the reports."
  task :delete_bo_reports, [] => :environment do |task, args|
    logger = Logger.new('log/cron.log')
    logger.info "Start!"
    # 通報いくつ以上で削除するか
    @report_count_limit = 3
    # 削除対象を抽出
    @reported_id = Report.where.not(received: 1).group(:word_id).having('count_id >= ?', @report_count_limit).count(:id)
    @reported_id.each do |r|
      # 該当の言葉を論理削除
      Word.where(id: r[0]).update_all(rejected: 1)
      # レポートデータを使用済みに
      Report.where(word_id: r[0]).update_all(received: 1)
      logger.info "Word:"+r[0].to_s+" rejected."
    end
    logger.info "End!"
  end
end

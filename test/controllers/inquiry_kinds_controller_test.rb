require 'test_helper'

class InquiryKindsControllerTest < ActionController::TestCase
  setup do
    @inquiry_kind = inquiry_kinds(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:inquiry_kinds)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create inquiry_kind" do
    assert_difference('InquiryKind.count') do
      post :create, inquiry_kind: { name: @inquiry_kind.name }
    end

    assert_redirected_to inquiry_kind_path(assigns(:inquiry_kind))
  end

  test "should show inquiry_kind" do
    get :show, id: @inquiry_kind
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @inquiry_kind
    assert_response :success
  end

  test "should update inquiry_kind" do
    patch :update, id: @inquiry_kind, inquiry_kind: { name: @inquiry_kind.name }
    assert_redirected_to inquiry_kind_path(assigns(:inquiry_kind))
  end

  test "should destroy inquiry_kind" do
    assert_difference('InquiryKind.count', -1) do
      delete :destroy, id: @inquiry_kind
    end

    assert_redirected_to inquiry_kinds_path
  end
end

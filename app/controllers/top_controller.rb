class TopController < ApplicationController
  def index
    @title = "キッズのことば"
    if user_signed_in? then
      logger.debug "top(login) session => "+session.to_json
      logger.debug "top(login) current_user => "+current_user.to_json
    else
      logger.debug "top(not login) session => "+session.to_json
      logger.debug "top(not login) current_user => "+current_user.to_json
    end
  end
end

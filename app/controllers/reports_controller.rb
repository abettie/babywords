class ReportsController < ApplicationController
  before_action :set_report, only: [:show, :edit, :update, :destroy]

  # GET /reports
  # GET /reports.json
  def index
  end

  # GET /reports/1
  # GET /reports/1.json
  def show
  end

  # GET /reports/new
  def new
  end

  # GET /reports/1/edit
  def edit
  end

  # POST /reports
  # POST /reports.json
  def create
    exists_flg = false
    # 同じGIPで通報済みかどうか
    if Report.exists?({ :gip => request.remote_ip, :word_id => params[:word_id] })
      #logger.debug "gip exists! "
      exists_flg = true
    end
    # 同じUserIDで通報済みかどうか
    if user_signed_in? && Report.exists?({ :user_id => current_user.id, :word_id => params[:word_id] })
      #logger.debug "uid exists! "
      exists_flg = true
    end

    if exists_flg == true
      # 通報済み
      render json: { status: "already" }
    else
      # 通報処理
      @report = Report.new(report_params)
      if @report.save
        render json: { status: "success" }
      else
        render json: { status: "error" }
      end
    end
  end

  # PATCH/PUT /reports/1
  # PATCH/PUT /reports/1.json
  def update
  end

  # DELETE /reports/1
  # DELETE /reports/1.json
  def destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_report
      @report = Report.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def report_params
      if user_signed_in? then
        params[:user_id] = current_user.id
      end
      params[:gip] = request.remote_ip
      params.permit(:word_id, :user_id, :gip)
    end
end

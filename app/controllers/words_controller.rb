class WordsController < ApplicationController
  before_action :set_word, only: [:show, :edit, :update, :destroy]

  # GET /words
  # GET /words.json
  def index
    # ニックネームも一緒に持ってくる
    @words = Word.where.not(rejected: 1).includes(:user_profile).all
    # logger.debug "word(index) @words => "+@words.pretty_inspect
  end

  # GET /words/1
  # GET /words/1.json
  def show; end

  # GET /words/new
  def new
    @word = Word.new
  end

  # GET /words/1/edit
  def edit; end

  # POST /words/page
  def page
    text = 'page:' + params['page'] + ' contain:' + params['contain']
    render text: text
  end

  # POST /words
  # POST /words.json
  def create
    @word = Word.new(word_params)

    respond_to do |format|
      if @word.save
        format.html { redirect_to @word, notice: 'Word was successfully created.' }
        format.json { render :show, status: :created, location: @word }
      else
        format.html { render :new }
        format.json { render json: @word.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /words/1
  # PATCH/PUT /words/1.json
  def update
    if user_signed_in?
      if @word.user_id != current_user.id
        # サインインユーザ以外のことばは編集禁止
        render nothing: true, status: 404
      else
        # ことばの所持ユーザなので編集許可
        respond_to do |format|
          if @word.update(word_params)
            format.html { redirect_to @word, notice: 'Word was successfully updated.' }
            format.json { render :show, status: :ok, location: @word }
          else
            format.html { render :edit }
            format.json { render json: @word.errors, status: :unprocessable_entity }
          end
        end
      end
    else
      # サインイン必須
      render nothing: true, status: 404
    end
  end

  # DELETE /words/1
  # DELETE /words/1.json
  def destroy
    @word.destroy
    respond_to do |format|
      format.html { redirect_to words_url, notice: 'Word was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_word
      @word = Word.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def word_params
      if user_signed_in? then
        params[:user_id] = current_user.id
      end
      params.permit(:id, :word, :translate, :comment, :age_year, :age_month, :gendar, :user_id, :rejected, :modified, :created, :datetime)
    end
end

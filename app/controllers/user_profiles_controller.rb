class UserProfilesController < ApplicationController
  before_action :set_user_profile, only: [:index, :show, :create, :update]

  # GET /user_profiles
  # GET /user_profiles.json
  def index
    # 一覧は見せず、セッションユーザのデータのみ見せる。
    show()
  end

  # GET /user_profiles/1
  # GET /user_profiles/1.json
  def show
    # indexから来る場合もあるので、actionを明示。
    if @user_profile.nil?
      render :nothing => true, :status => 200
    else
      render action: :show
    end
  end

  # GET /user_profiles/new
  def new
    # 使わない
    render :nothing => true, :status => 404
  end

  # GET /user_profiles/1/edit
  def edit
    # 使わない
    render :nothing => true, :status => 404
  end

  # POST /user_profiles
  # POST /user_profiles.json
  def create
    # セッションユーザデータの編集のみ
    update()
  end

  # PATCH/PUT /user_profiles/1
  # PATCH/PUT /user_profiles/1.json
  def update
    #logger.debug "params:"+params.to_json
    if @user_profile.update(nickname: params[:nickname])
      # 何かjsonを返さないとbackboneのmodel.saveで失敗するため。 
      render json: '{"result": "success"}'
    else
      logger.error "user_profile update error! "+params.to_json
      head 500
    end
  end

  # DELETE /user_profiles/1
  # DELETE /user_profiles/1.json
  def destroy
    # 使わない
    render :nothing => true, :status => 404
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_profile
      # セッションからid取得
      if user_signed_in? then
        @user_profile = UserProfile.find_by(user_id: current_user.id)
      else
        @user_profile = nil
      end
    end
end

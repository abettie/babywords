require 'active_support/concern'

module UserAgent 
  extend ActiveSupport::Concern

  #
  # スマートフォンかどうかの判定
  #
  def is_sp?
    request.user_agent =~ /(iPhone|iPod)+/ || request.user_agent =~ /Android.*Mobile/
  end
end

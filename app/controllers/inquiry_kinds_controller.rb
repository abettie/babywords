class InquiryKindsController < ApplicationController
  before_action :set_inquiry_kind, only: [:show, :edit, :update, :destroy]

  # GET /inquiry_kinds
  # GET /inquiry_kinds.json
  def index
    @inquiry_kinds = InquiryKind.all
  end

  # GET /inquiry_kinds/1
  # GET /inquiry_kinds/1.json
  def show
  end

  # GET /inquiry_kinds/new
  def new
    @inquiry_kind = InquiryKind.new
  end

  # GET /inquiry_kinds/1/edit
  def edit
  end

  # POST /inquiry_kinds
  # POST /inquiry_kinds.json
  def create
    @inquiry_kind = InquiryKind.new(inquiry_kind_params)

    respond_to do |format|
      if @inquiry_kind.save
        format.html { redirect_to @inquiry_kind, notice: 'Inquiry kind was successfully created.' }
        format.json { render :show, status: :created, location: @inquiry_kind }
      else
        format.html { render :new }
        format.json { render json: @inquiry_kind.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /inquiry_kinds/1
  # PATCH/PUT /inquiry_kinds/1.json
  def update
    respond_to do |format|
      if @inquiry_kind.update(inquiry_kind_params)
        format.html { redirect_to @inquiry_kind, notice: 'Inquiry kind was successfully updated.' }
        format.json { render :show, status: :ok, location: @inquiry_kind }
      else
        format.html { render :edit }
        format.json { render json: @inquiry_kind.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /inquiry_kinds/1
  # DELETE /inquiry_kinds/1.json
  def destroy
    @inquiry_kind.destroy
    respond_to do |format|
      format.html { redirect_to inquiry_kinds_url, notice: 'Inquiry kind was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_inquiry_kind
      @inquiry_kind = InquiryKind.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def inquiry_kind_params
      params.require(:inquiry_kind).permit(:name)
    end
end

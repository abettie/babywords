class ApplicationController < ActionController::Base
  require 'pp'
  include UserAgent

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :detect_device

  private
    #
    # スマートフォンかどうかの判定
    #
    def detect_device
      if is_sp?
        #request.variant = :sp
      else
        #request.variant = :pc
      end
    end

  # twitterログインキャンセル時の挙動
  def new_session_path(scope)
    # TOPへ戻る
    return "/"
  end
end

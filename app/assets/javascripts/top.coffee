# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

##################################################
# コレクション、モデルをここで用意
##################################################
# フェッチするコレクションの数
c_count_to_fetch = 2

# モデル - ことば
Www.model.my_word = new Www.Models.Word()

# コレクション - ことば
Www.collection.words = new Www.Collections.Words()

# モデル - 問い合わせ
Www.model.my_inquiry = new Www.Models.Inquiry()

# モデル - 問い合わせ種別
Www.model.inquiry_kind = new Www.Models.InquiryKind()

# コレクション - 問い合わせ種別
Www.collection.inquiry_kinds = new Www.Collections.InquiryKinds()

# モデル - ユーザープロフィール
Www.model.user_profile = new Www.Models.UserProfile()

$ ->
  ############################################
  # 全てのコレクションフェッチ後の処理(初期処理)
  ############################################
  afterFetch = ->
    # コレクション準備
    Www.collection.words.myRefresh()

    ############################################
    # 全てのビューの用意
    ############################################
    # メイン画面の表示
    Www.view.main = new Www.Views.Main()
    Www.view.main.render()

    # ヘッダーViewの用意
    Www.view.header = new Www.Views.Header()

    # メニューブロックの用意
    Www.view.menu = new Www.Views.Menu()

    # メニューダイアログの用意
    Www.view.menu_dialog = new Www.Views.MenuDialog()

    # ニックネームダイアログの用意
    Www.view.nickname_dialog = new Www.Views.NicknameDialog()

    # 通報ダイアログの用意
    Www.view.report_dialog = new Www.Views.ReportDialog()

    # 編集ダイアログの用意
    Www.view.edit_dialog = new Www.Views.EditDialog()

    # 編集完了ダイアログの用意
    Www.view.edit_complete = new Www.Views.EditComplete()

    # 問い合わせダイアログの用意
    Www.view.inquiry_dialog = new Www.Views.InquiryDialog()

    # このサイトについてダイアログの用意
    Www.view.about_dialog = new Www.Views.AboutDialog()

    # 投稿ダイアログの用意
    Www.view.post_dialog = new Www.Views.PostDialog()

    # 投稿完了ダイアログの用意
    Www.view.post_complete = new Www.Views.PostComplete()

    # ログインダイアログの用意
    Www.view.login_dialog =  new Www.Views.LoginDialog()

    # 利用規約ダイアログの用意
    Www.view.agreement_dialog = new Www.Views.AgreementDialog()

    # 汎用ダイアログの用意
    Www.view.common_dialog = new Www.Views.CommonDialog()

    if Www.var.signed_in == 1
      if Www.model.user_profile.get("nickname") == null
        # ニックネーム登録画面表示
        Www.view.nickname_dialog.render()
      else
        # 登録済みのニックネームを投稿画面に反映
        $("#post-dialog-nickname").text(Www.model.user_profile.get("nickname"))

    if Www.var.signed_in == 1
      # もしログインしていたらログアウトリンクを表示
      $("#header-to-logout").css("display", "block")
    else
      # もしログアウトしていたらログインリンクを表示
      $("#header-to-login").css("display", "block")

    # プログレスダイアログを閉じる
    Www.mediator.trigger("progress-dialog:close")

  ############################################
  # プログレスダイアログ
  ############################################
  # プログレスダイアログは早めに用意
  Www.view.progress_dialog =  new Www.Views.ProgressDialog()

  # プログレスダイアログ表示
  Www.mediator.trigger("progress-dialog:render")

  ############################################
  # ユーザプロフィール更新
  ############################################
  Www.var.signed_in = $("body").data("signed-in")

  if Www.var.signed_in == 1
    # もしログインしていたらユーザプロフィールもフェッチ
    c_count_to_fetch++
    Www.model.user_profile.fetch(
      success: (model, response, options)->
        fetchCounter()
      error: (model, response, options)->
        modelFetchFailed(model, response, options)
    )


  # モデルフェッチ失敗時の処理
  modelFetchFailed = (model, response, options)->
    Www.mediator.trigger("progress-dialog:close")

  ############################################
  # コレクションをフェッチ
  ############################################
  # 非同期処理をカウントするクロージャ
  clAsyncCounter = (expectedCount, callback)->
    count = 0
    return ->
      count++
      if count == expectedCount
        callback()
    
  # コレクションフェッチ失敗時の処理
  collectionFetchFailed = (collection, response, options)->
    Www.mediator.trigger("progress-dialog:close")
  
  # 非同期フェッチをカウント
  fetchCounter = clAsyncCounter(c_count_to_fetch, afterFetch)

  # 各コレクションのフェッチ(非同期)
  Www.collection.words.fetch(
    success: (collection, response, options)->
      fetchCounter()
    error: (collection, response, options)->
      collectionFetchFailed(collection, response, options)
  )
  Www.collection.inquiry_kinds.fetch(
    success: (collection, response, options)->
      fetchCounter()
    error: (collection, response, options)->
      collectionFetchFailed(collection, response, options)
  )



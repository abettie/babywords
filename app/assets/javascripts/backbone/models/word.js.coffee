class Www.Models.Word extends Backbone.Model
  urlRoot: 'words'

  defaults:
    id: null
    word: null
    translate: null
    comment: null
    age_year: null
    age_month: null
    gendar: null
    rejected: 0

  validate: (attrs, options)->
    err_msg = ""
    if (!attrs.gendar) 
      err_msg += "性別を選択してください。<br>"
    if (attrs.word.match(/^$/)) 
      err_msg += "幼児言葉を入力してください。<br>"
    else if (!attrs.word.match(/^[ぁ-ん、。～…・！？ー]+$/)) 
      err_msg += "幼児言葉はひらがなと「、。～…！？ー」のみでお願いします。<br>"
    if (attrs.translate.match(/^$/)) 
      err_msg += "大人語訳を入力してください。<br>"
    return err_msg

class Www.Collections.Words extends Backbone.MyCollection
  model: Www.Models.Word
  url: '/words'

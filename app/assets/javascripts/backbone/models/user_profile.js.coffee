class Www.Models.UserProfile extends Backbone.Model
  urlRoot: 'user_profiles'

  defaults:
    user_id: null
    nickname: null

  validate: (attrs, options)->
    err_msg = ""
    if (attrs.nickname.match(/^$/)) 
      err_msg += "ニックネームを入力してください。<br>"
    if (attrs.nickname == "管理人") 
      err_msg += "ニックネームに「管理人」は使用できません。<br>"
    return err_msg

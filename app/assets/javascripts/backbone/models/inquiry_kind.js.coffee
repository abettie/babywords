class Www.Models.InquiryKind extends Backbone.Model
  urlRoot: 'inquiry_kinds'

  defaults:
    name: null

class Www.Collections.InquiryKinds extends Backbone.MyCollection
  model: Www.Models.InquiryKind
  url: '/inquiry_kinds'

class Www.Models.Inquiry extends Backbone.Model
  urlRoot: 'inquiries'

  defaults:
    kind_id: null
    body: null

  validate: (attrs, options)->
    err_msg = ""
    if (!attrs.kind_id) 
      err_msg += "種別を選択してください。<br>"
    if (attrs.body.match(/^$/)) 
      err_msg += "内容を入力してください。<br>"
    return err_msg

#= require_self
#= require_tree ./templates
#= require_tree ./models
#= require_tree ./views
#= require_tree ./routers

window.Www =
  Models: {}
  Collections: {}
  Routers: {}
  Views: {}
  model: {}
  collection: {}
  view: {}
  var: {}
  const: {}

# 拡張コレクションを自作
Backbone.MyCollection = Backbone.Collection.extend({
  initialize: ()-> 
  ,

  # 現在のモデルID
  myCurrentId: null,

  # 現在のシーケンス
  myCurrentSeq: null,

  # 絞込後のモデル配列
  myFilteredArr: [],

  # 現在の絞り込み、ソート設定でリフレッシュ
  myRefresh: ->
    # 絞込
    filter = {}
    if this.myFilter.key != null
      filter[this.myFilter.key] = this.myFilter.val
    this.myFilteredArr = this.where(filter)
    # ソート
    sorted = []
    if this.mySortOrder == "asc"
      # 昇順
      sorted = _.sortBy(this.myFilteredArr, (m)->
        m.id
      )
    else
      # 降順
      sorted = _.sortBy(this.myFilteredArr, (m)->
        -m.id
      )
    this.myFilteredArr = sorted
    this.myCurrentSeq = 0
    this.myCurrentId = this.myFilteredArr[this.myCurrentSeq]["id"]
    this.trigger("refresh")
  ,

  # 現在の絞り込み、ソートを維持してDBからリロード
  myReload: ->
    this.myFetch()
  ,

  # 読み込み中ダイアログを出しつつ、DB同期 
  myFetch: ->
    Www.mediator.trigger("progress-dialog:render")
    that = this
    this.fetch(
      success: (collection, res, options)->
        Www.mediator.trigger("progress-dialog:close")
        that.myRefresh()
      ,
      error: ()->
        Www.mediator.trigger("progress-dialog:close")
      ,
    )
  ,

  # ソート順
  mySortOrder: "asc",

  # ソート順セット
  mySetSortOrder: (order)->
    this.mySortOrder = order
    this.myRefresh()
  ,

  # ソート順トグル
  myToggleSortOrder: ()->
    if this.mySortOrder == "asc"
      this.mySetSortOrder("desc")
    else
      this.mySetSortOrder("asc")
  ,

  # 絞り込み条件
  myFilter: 
    key: null,
    val: null,
    com: "eq",
  ,

  # 絞り込み条件セット
  mySetFilter: (key, val, com="eq")->
    this.myFilter.key = key
    this.myFilter.val = val
    this.myFilter.com = com
    this.myRefresh()
  ,

  # 絞り込み条件クリア
  myClearFilter: ()->
    this.myFilter.key = null
    this.myFilter.val = null
    this.myFilter.com = "eq"
    this.myRefresh()
  ,

  # ページング：前のページへ
  myGoPrev: ->
    this.myCurrentSeq = this.myCurrentSeq - 1
    this.myCurrentId = this.myFilteredArr[this.myCurrentSeq]["id"]
    this.trigger("seq-change")
  ,

  # ページング：次のページへ
  myGoNext: ->
    this.myCurrentSeq = this.myCurrentSeq + 1
    this.myCurrentId = this.myFilteredArr[this.myCurrentSeq]["id"]
    this.trigger("seq-change")
  ,

  # ページング：最初のページへ
  myGoFirst: ->
    this.myCurrentSeq = 0
    this.myCurrentId = this.myFilteredArr[this.myCurrentSeq]["id"]
    this.trigger("seq-change")
  ,

  # ページング：最後のページへ
  myGoLast: ->
    this.myCurrentSeq = this.myFilteredArr.length - 1
    this.myCurrentId = this.myFilteredArr[this.myCurrentSeq]["id"]
    this.trigger("seq-change")
  ,

  # ページング：次のページがあるかどうか
  myHasNext: ->
    if this.myCurrentSeq >= this.myFilteredArr.length-1
      return false
    else
      return true
  ,

  # ページング：前のページがあるかどうか
  myHasPrev: ->
    if this.myCurrentSeq <= 0
      return false
    else
      return true
  ,
})

window.Www.mediator = _.extend({}, Backbone.Events)

# 規約同意済みフラグをクッキーに保存するキー
window.Www.const.agreement_key = "agreement"

# 規約のバージョン
window.Www.const.agreement_version = 1

# 男女の日本語
window.Www.const.gendar_jp = { "m": "男の子", "f": "女の子"}


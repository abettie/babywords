class Www.Routers.WordsRouter extends Backbone.Router
  initialize: (options) ->
    @words = new Www.Collections.WordsCollection()
    @words.reset options.words

  routes:
    "new"      : "newWord"
    "index"    : "index"
    ":id/edit" : "edit"
    ":id"      : "show"
    ".*"        : "index"

  newWord: ->
    @view = new Www.Views.Words.NewView(collection: @words)
    $("#words").html(@view.render().el)

  index: ->
    @view = new Www.Views.Words.IndexView(collection: @words)
    $("#words").html(@view.render().el)

  show: (id) ->
    word = @words.get(id)

    @view = new Www.Views.Words.ShowView(model: word)
    $("#words").html(@view.render().el)

  edit: (id) ->
    word = @words.get(id)

    @view = new Www.Views.Words.EditView(model: word)
    $("#words").html(@view.render().el)

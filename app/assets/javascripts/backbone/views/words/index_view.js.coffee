Www.Views.Words ||= {}

class Www.Views.Words.IndexView extends Backbone.View
  template: JST["backbone/templates/words/index"]

  initialize: () ->
    @collection.bind('reset', @addAll)

  addAll: () =>
    @collection.each(@addOne)

  addOne: (word) =>
    view = new Www.Views.Words.WordView({model : word})
    @$("tbody").append(view.render().el)

  render: =>
    @$el.html(@template(words: @collection.toJSON() ))
    @addAll()

    return this

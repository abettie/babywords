class Www.Views.InquiryDialog extends Backbone.View
  el: "#inquiry-dialog"

  events:
    # 送信ボタンを押す
    "click #inquiry-dialog-post-submit": "post_submit"
    # クローズアイコンを押す
    "click #inquiry-dialog-close-icon": "close_dialog"
  
  initialize: ->
    # 表示要求イベントを監視
    this.listenTo(Www.mediator, "inquiry-dialog:render", this.render)

  post_submit: ->
    # フォームに入力された値をモデルに適用
    Www.model.my_inquiry.set(
      kind_id: this.$("#inquiry-dialog-input-kind").val()
      body: this.$("#inquiry-dialog-input-body").val()
    )
    # バリデーション
    if (!Www.model.my_inquiry.isValid()) 
      this.$("#inquiry-dialog-error-message").html(Www.model.my_inquiry.validationError);
      this.$("#inquiry-dialog-post-form").scrollTop(0)
    else
      Www.mediator.trigger("progress-dialog:render")
      Www.model.my_inquiry.save({},
        success: (m, r, o)=>
          this.$el.css("display", "none")
          this.clear()
          Www.mediator.trigger("progress-dialog:close")
          Www.mediator.trigger("common-dialog:render", "送信完了", "問い合わせの送信が完了しました。")
        error: (m, r, o)=>
          # 送信失敗!!
          this.clear()
          Www.mediator.trigger("progress-dialog:close")
          window.location.href = "/error/logout"
          exit
        wait: true
      )

  # モデルを初期化（idがキャッシュされて次の投稿が編集にならないように）
  clear: ->
    Www.model.my_inquiry.clear()
    this.$el.find("textarea, :text").val("")

  close_dialog: ->
    this.$el.css("display", "none")
    this.$el.children("div").css("display", "none")

  render: ->
    # フォーム調整
# TODO Www.collection.inquiry_kindsでselectタグのoption追加
    $("#inquiry-dialog-input-kind").empty()
    Www.collection.inquiry_kinds.each((item)->
      $("#inquiry-dialog-input-kind").append($("<option>").val(item.get("id")).text(item.get("name")))
    )
    this.$("#inquiry-dialog-input-kind").val(null)
    this.$("#inquiry-dialog-input-body").val(null)

    # フェードインで表示
    this.$el.css("display", "flex")
    this.$el.children("div").css("display", "none").fadeIn("short")

class Www.Views.NicknameDialog extends Backbone.View
  el: "#nickname-dialog"

  events:
    # 投稿ボタンを押す
    "click #nickname-dialog-submit": "post_submit"
  
  initialize: ->
    # 表示要求イベントを監視
    this.listenTo(Www.mediator, "nickname-dialog:render", this.render)

  post_submit: ->
    # フォームに入力された値をモデルに適用
    Www.model.user_profile.set(
      nickname: this.$("#nickname-dialog-input-nickname").val()
    )
    # バリデーション
    if (!Www.model.user_profile.isValid()) 
      this.$("#nickname-dialog-error-message").html(Www.model.user_profile.validationError);
      this.$el.scrollTop(0)
    else
      Www.mediator.trigger("progress-dialog:render")
      Www.model.user_profile.save({},
        success: (m, r, o)=>
          Www.mediator.trigger("progress-dialog:close")
          this.close_dialog()
          Www.collection.words.myReload()
          Www.mediator.trigger("common-dialog:render", "登録完了", "ニックネームの登録が完了しました。")
          # 投稿画面のニックネームに反映
          $("#post-dialog-nickname").text(Www.model.user_profile.get("nickname"))
        error: (m, r, o)=>
          Www.mediator.trigger("progress-dialog:close")
          # 登録失敗!!ログインしなおしてもらう
          window.location.href = "/error/logout"
          exit
        wait: true
      )

  close_dialog: ->
    this.$el.css("display", "none")
    this.$el.children("div").css("display", "none")

  render: ->
    # ニックネームを初期表示
    this.$el.css("display", "flex")
    this.$el.children("div").css("display", "none").fadeIn("short")

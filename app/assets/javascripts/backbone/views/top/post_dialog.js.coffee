class Www.Views.PostDialog extends Backbone.View
  el: "#post-dialog"

  events:
    # 投稿ボタンを押す
    "click #post-dialog-post-submit": "post_submit"
    # 戻るボタンを押す
    "click #post-dialog-input-return": "close_dialog"
    # クローズアイコンを押す
    "click #post-dialog-close-icon": "close_dialog"
  
  initialize: ->
    # 表示要求イベントを監視
    this.listenTo(Www.mediator, "post-dialog:render", this.render)

  post_submit: ->
    # フォームに入力された値をモデルに適用
    Www.model.my_word.set(
      age_year: this.$("#post-dialog-input-age-year").val()
      age_month: this.$("#post-dialog-input-age-month").val()
      gendar: this.$("input[name='gender']:checked").val()
      word: this.$("#post-dialog-input-word").val()
      translate: this.$("#post-dialog-input-translate").val()
      comment: this.$("#post-dialog-input-comment").val()
    )
    # バリデーション
    if (!Www.model.my_word.isValid()) 
      this.$("#post-dialog-error-message").html(Www.model.my_word.validationError);
      this.$("#post-dialog-post-form").scrollTop(0)
    else
      Www.mediator.trigger("progress-dialog:render")
      Www.model.my_word.save({},
        success: (m, r, o)=>
          this.$el.css("display", "none")
          this.clear()
          Www.mediator.trigger("progress-dialog:close")
          Www.mediator.trigger("post-complete:render")
        error: (m, r, o)=>
          # 投稿失敗!!
          this.clear()
          Www.mediator.trigger("progress-dialog:close")
          window.location.href = "/error/logout"
          exit
        wait: true
      )

  # モデルを初期化（idがキャッシュされて次の投稿が編集にならないように）
  clear: ->
    Www.model.my_word.clear()
    this.$el.find("textarea, :text").val("")
    

  close_dialog: ->
    this.$el.css("display", "none")
    this.$el.children("div").css("display", "none")

  render: ->
    # ビュー調整
    this.$("#post-dialog-input-title").text("ことば投稿")
    this.$("#post-dialog-message").html("<div>投稿ありがとうございます！</div><div>以下、ご入力お願いします。</div>")
    this.$("#post-dialog-message-for-admin").css("display", "block")
    this.$("#post-dialog-post-submit").css("display", "block")
    this.$("#post-dialog-edit-submit").css("display", "none")

    # フォーム調整
    this.$("#post-dialog-input-age-year").val(null)
    this.$("#post-dialog-input-age-month").val(null)
    this.$("input[name=gender]").val([])
    this.$("#post-dialog-input-word").val(null)
    this.$("#post-dialog-input-translate").val(null)
    this.$("#post-dialog-input-comment").val(null)

    # フェードインで表示
    this.$el.css("display", "flex")
    this.$el.children("div").css("display", "none").fadeIn("short")

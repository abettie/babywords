# テンプレートはPostDialogと共用
class Www.Views.EditDialog extends Backbone.View
  el: "#post-dialog"

  events:
    # 投稿ボタンを押す
    "click #post-dialog-edit-submit": "edit_submit"
    # 戻るボタンを押す
    "click #post-dialog-input-return": "close_dialog"
    # クローズアイコンを押す
    "click #post-dialog-close-icon": "close_dialog"
  
  initialize: ->
    # 表示要求イベントを監視
    this.listenTo(Www.mediator, "edit-dialog:render", this.render)

  # ターゲットモデル
  target_model: null

  edit_submit: ->
    # フォームに入力された値をモデルに適用
    this.target_model.set(
      age_year: this.$("#post-dialog-input-age-year").val()
      age_month: this.$("#post-dialog-input-age-month").val()
      gendar: this.$("input[name='gender']:checked").val()
      word: this.$("#post-dialog-input-word").val()
      translate: this.$("#post-dialog-input-translate").val()
      comment: this.$("#post-dialog-input-comment").val()
    )
    # バリデーション
    if (!this.target_model.isValid()) 
      this.$("#post-dialog-error-message").html(this.target_model.validationError);
      this.$("#post-dialog-post-form").scrollTop(0)
    else
      Www.mediator.trigger("progress-dialog:render")
      this.target_model.save({},
        success: (m, r, o)=>
          this.$el.css("display", "none")
          this.clear()
          Www.mediator.trigger("progress-dialog:close")
          Www.mediator.trigger("edit-complete:render")
        error: (m, r, o)=>
          # 投稿失敗!!
          this.clear()
          Www.mediator.trigger("progress-dialog:close")
          window.location.href = "/error/logout"
          exit
        wait: true
      )

  # モデルを初期化（idがキャッシュされて次の投稿が編集にならないように）
  clear: ->
    this.target_model.clear()
    this.$el.find("textarea, :text").val("")
    

  close_dialog: ->
    this.$el.css("display", "none")
    this.$el.children("div").css("display", "none")

  render: ->
    # ビュー調整
    this.$("#post-dialog-input-title").text("ことば編集")
    this.$("#post-dialog-message").html("<div>内容を変更します。</div>")
    this.$("#post-dialog-message-for-admin").css("display", "none")
    this.$("#post-dialog-post-submit").css("display", "none")
    this.$("#post-dialog-edit-submit").css("display", "block")

    # 編集対象のモデル取得
    this.target_model = Www.collection.words.get(Www.collection.words.myCurrentId)
    console.log this.target_model
    
    # フォーム調整
    this.$("#post-dialog-input-age-year").val(this.target_model.get("age_year"))
    this.$("#post-dialog-input-age-month").val(this.target_model.get("age_month"))
    this.$("input[name=gender]").val([this.target_model.get("gendar")])
    this.$("#post-dialog-input-word").val(this.target_model.get("word"))
    this.$("#post-dialog-input-translate").val(this.target_model.get("translate"))
    this.$("#post-dialog-input-comment").val(this.target_model.get("comment"))

    # フェードインで表示
    this.$el.css("display", "flex")
    this.$el.children("div").css("display", "none").fadeIn("short")

class Www.Views.Header extends Backbone.View
  el: "#header"

  events:
    # ログインボタンを押す
    "click #header-to-login": "toLogin"
    # メニューアイコンを押す
    "click #header-menu-icon": "displayMenu"
  
  initialize: ->

  toLogin: ->
    # ログイン画面へ
    Www.mediator.trigger("login-dialog:render")

  displayMenu: ->
    # メニュー画面表示
    Www.mediator.trigger("menu-dialog:render")


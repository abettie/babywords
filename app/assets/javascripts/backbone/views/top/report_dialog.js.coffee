class Www.Views.ReportDialog extends Backbone.View
  el: "#report-dialog"

  events:
    # 通報するボタンを押す
    "click #report-dialog-submit": "submit"
    # キャンセルボタンを押す
    "click #report-dialog-cancel": "close_dialog"
  
  initialize: ->
    # 表示要求イベントを監視
    this.listenTo(Www.mediator, "report-dialog:render", this.render)

  submit: ->
    Www.mediator.trigger("progress-dialog:render")
    that = this
    post_data = {
      word_id: Www.collection.words.myCurrentId
    }
    jqXHR = $.ajax(
      url: '/reports',
      type: 'post',
      dataType: 'json',
      data: post_data
    )
    jqXHR.done (data, stat, xhr) ->
      that.close_dialog()
      if data.status == "success"
        Www.mediator.trigger("common-dialog:render", "通報完了", "通報しました。")
      else if data.status == "already"
        Www.mediator.trigger("common-dialog:render", "通報済み", "既に通報済みです。")
      else
        Www.mediator.trigger("common-dialog:render", "通報失敗", "通報に失敗しました。")
    jqXHR.fail (xhr, stat, err) ->
      that.close_dialog()
      Www.mediator.trigger("common-dialog:render", "通報失敗", "通報に失敗しました。")

  close_dialog: ->
    this.$el.css("display", "none")
    this.$el.children("div").css("display", "none")
    Www.mediator.trigger("progress-dialog:close")

  render: ->
    this.$el.css("display", "flex")
    this.$el.children("div").css("display", "none").fadeIn("short")

class Www.Views.AgreementDialog extends Backbone.View
  el: "#agreement-dialog"

  events:
    # 同意するボタンを押す
    "click #agreement-dialog-ok": "to_login"
    # 同意しないボタンを押す
    "click #agreement-dialog-ng": "close_dialog"
    # 閉じるボタンを押す
    "click #agreement-dialog-close": "close_dialog"
    # クローズアイコンを押す
    "click #agreement-dialog-close-icon": "close_dialog"
  
  initialize: ->
    # 表示要求イベントを監視
    this.listenTo(Www.mediator, "agreement-dialog:render", this.render)
    this.listenTo(Www.mediator, "agreement-dialog:render_without_button", this.render_without_button)

  to_login: ->
    # クッキーに同意済みフラグを保存
    $.cookie(Www.const.agreement_key, Www.const.agreement_version, { expires: 365, path: "/", secure: true})
    # ログインへ遷移する
    location.href = Www.const.url_login

  close_dialog: ->
    this.$el.css("display", "none")
    this.$el.children("div").css("display", "none")

  render: ->
    # ボタンを表示
    this.$("#agreement-dialog-confirm").css("display", "block")
    this.$("#agreement-dialog-ok").css("display", "block")
    this.$("#agreement-dialog-ng").css("display", "block")
    this.$("#agreement-dialog-close").css("display", "none")
    # フェードインで表示
    this.$el.css("display", "flex")
    this.$el.children("div").css("display", "none").fadeIn("short")

  render_without_button: ->
    # ボタンを非表示
    this.$("#agreement-dialog-confirm").css("display", "none")
    this.$("#agreement-dialog-ok").css("display", "none")
    this.$("#agreement-dialog-ng").css("display", "none")
    this.$("#agreement-dialog-close").css("display", "block")
    # フェードインで表示
    this.$el.css("display", "flex")
    this.$el.children("div").css("display", "none").fadeIn("short")

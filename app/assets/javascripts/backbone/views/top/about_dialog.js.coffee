class Www.Views.AboutDialog extends Backbone.View
  el: "#about-dialog"

  events:
    # クローズアイコンを押す
    "click #about-dialog-close-icon": "close_dialog"
    # 閉じるボタンを押す
    "click #about-dialog-close-button": "close_dialog"

  initialize: ->
    # 表示要求イベントを監視
    this.listenTo(Www.mediator, "about-dialog:render", this.render)

  close_dialog: ->
    this.$el.css("display", "none")
    this.$el.children("div").css("display", "none")

  render: ->
    # フェードインで表示
    this.$el.css("display", "flex")
    this.$el.children("div").css("display", "none").fadeIn("short")

class Www.Views.EditComplete extends Backbone.View
  el: "#edit-complete"

  events:
    # 確認ボタンを押す
    "click #edit-complete-return": "close_dialog"
    # クローズアイコンを押す
    "click #edit-complete-close-icon": "close_dialog"
  
  initialize: ->
    # 表示要求イベントを監視
    this.listenTo(Www.mediator, "edit-complete:render", this.render)

  close_dialog: ->
    this.$el.css("display", "none")
    Www.collection.words.myReload()

  render: ->
    this.$el.css("display", "flex")
    this.$("#edit-complete-message").css("display", "none").fadeIn("short")

class Www.Views.MenuDialog extends Backbone.View
  el: "#menu-dialog"

  events:
    # クローズアイコンを押す
    "click #menu-dialog-close-icon": "close_dialog"
    # このサイトについて
    "click .menu .about": "show_about"
    # 利用規約
    "click .menu .agreement": "show_agreement"
    # 問い合わせ
    "click .menu .inquiry": "show_inquiry"
  
  initialize: ->
    # 表示要求イベントを監視
    this.listenTo(Www.mediator, "menu-dialog:render", this.render)

  close_dialog: ->
    this.$el.css("display", "none")
    this.$el.children("div").css("display", "none")

  show_about: ->
    this.close_dialog()
    Www.mediator.trigger("about-dialog:render")

  show_agreement: ->
    this.close_dialog()
    Www.mediator.trigger("agreement-dialog:render_without_button")

  show_inquiry: ->
    Www.mediator.trigger("inquiry-dialog:render")

  render: ->
    this.$el.css("display", "flex")
    this.$el.children("div").css("display", "none").fadeIn("short")


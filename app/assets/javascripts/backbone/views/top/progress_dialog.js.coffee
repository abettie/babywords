class Www.Views.ProgressDialog extends Backbone.View
  el: "#progress-dialog"

  initialize: ->
    # 表示要求イベントを監視
    this.listenTo(Www.mediator, "progress-dialog:render", this.render)
    this.listenTo(Www.mediator, "progress-dialog:close", this.close)

  close: ->
    this.$el.css("display", "none")

  render: ->
    this.$el.css("display", "flex")

class Www.Views.CommonDialog extends Backbone.View
  el: "#common-dialog"

  events:
    # クローズアイコンを押す
    "click #common-dialog-close-icon": "close_dialog"
  
  initialize: ->
    # 表示要求イベントを監視
    this.listenTo(Www.mediator, "common-dialog:render", this.render)

  close_dialog: ->
    this.$el.css("display", "none")
    this.$el.children("div").css("display", "none")

  render: (title, message)->
    this.$el.find("#common-dialog-title").html(title)
    this.$el.find("#common-dialog-message").html(message)
    this.$el.css("display", "flex")
    this.$el.children("div").css("display", "none").fadeIn("short")


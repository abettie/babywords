class Www.Views.PostComplete extends Backbone.View
  el: "#post-complete"

  events:
    # 確認ボタンを押す
    "click #post-complete-return": "close_dialog"
    # クローズアイコンを押す
    "click #post-complete-close-icon": "close_dialog"
  
  initialize: ->
    # 表示要求イベントを監視
    this.listenTo(Www.mediator, "post-complete:render", this.render)

  close_dialog: ->
    this.$el.css("display", "none")
    Www.collection.words.myReload()

  render: ->
    this.$el.css("display", "flex")
    this.$("#post-complete-message").css("display", "none").fadeIn("short")

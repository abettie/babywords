class Www.Views.Main extends Backbone.View
  el: "#main-block"

  events:
    # 昇順、降順ボタンを押す
    "click #main-block-toggle-sort": "toggleSort"
    # my投稿で絞り込む
    "click #main-block-filter-my-word": "filterMyWord"
    # 通報ボタンを押す
    "click #main-block-report": "report"
    # 編集ボタンを押す
    "click #main-block-edit": "edit"
    # 前へボタンを押す
    "click #main-block-prev": "prev"
    # 次へボタンを押す
    "click #main-block-next": "next"
    # 最初へボタンを押す
    "click #main-block-first": "first"
    # 最後へボタンを押す
    "click #main-block-last": "last"
    # 投稿ボタンを押す
    "click #main-block-post-word": "postWord"
  
  initialize: ->
    this.listenTo(Www.collection.words, "seq-change", this.render)
    this.listenTo(Www.collection.words, "refresh", this.render)

  toggleSort: ->
    # ソート順トグル
    Www.collection.words.myToggleSortOrder()

  filterMyWord: ->
    if Www.var.signed_in == 1
      # 自分の投稿で絞り込む（トグル）
      if Www.collection.words.myFilter.key == null
        Www.collection.words.mySetFilter("user_id", Www.model.user_profile.get("user_id"))
      else
        Www.collection.words.myClearFilter()
    else
      # ログイン催促ダイアログ表示
      Www.mediator.trigger("login-dialog:render")

  report: ->
    # 通報ダイアログ表示
    Www.mediator.trigger("report-dialog:render")

  edit: ->
    # 編集ダイアログ表示
    Www.mediator.trigger("edit-dialog:render")

  prev: ->
    # 前の言葉へ
    Www.collection.words.myGoPrev()

  next: ->
    # 次の言葉へ
    Www.collection.words.myGoNext()

  first: ->
    # 最初の言葉へ
    Www.collection.words.myGoFirst()

  last: ->
    # 最後の言葉へ
    Www.collection.words.myGoLast()
    $.ajax(
      url: '/words/page',
      type: 'POST',
      data:
        page: 1,
        contain: 10,
    )
    .done((data)->
      console.log data
    )
    .fail((data)->
      console.log 'ajax failed!'
      console.log data
    );


  postWord: ->
    if Www.var.signed_in == 1
      # 投稿ダイアログ表示
      Www.mediator.trigger("post-dialog:render")
    else
      # ログイン催促ダイアログ表示
      Www.mediator.trigger("login-dialog:render")

  render: ->
    # IDに応じて次へ前へ最初へ最後へボタンを無効化する
    if !Www.collection.words.myHasPrev()
      @$("#main-block-prev").prop("disabled", true)
      @$("#main-block-first").prop("disabled", true)
    else
      @$("#main-block-prev").prop("disabled", false)
      @$("#main-block-first").prop("disabled", false)
    if !Www.collection.words.myHasNext()
      @$("#main-block-next").prop("disabled", true)
      @$("#main-block-last").prop("disabled", true)
    else
      @$("#main-block-next").prop("disabled", false)
      @$("#main-block-last").prop("disabled", false)

    # 絞込に応じて表示変更
    if Www.collection.words.myFilter.key == null 
      @$("#main-block-filter-my-word").text("全ての投稿")
    else
      @$("#main-block-filter-my-word").text("自分の投稿")

    # ソート順に応じて表示変更
    if Www.collection.words.mySortOrder == "asc"
      @$("#main-block-toggle-sort").text("昇順")
    else
      @$("#main-block-toggle-sort").text("降順")

    # 現在のIDのwordモデルを取得し、表示を書き換える
    tmp_word = Www.collection.words.get(Www.collection.words.myCurrentId)
    @$("#main-block-babyword").text(tmp_word.get("word"))
    @$("#main-block-author").text(tmp_word.get("nickname"))
    @$("#main-block-age-year").text(tmp_word.get("age_year"))
    age_month = tmp_word.get("age_month")
    if age_month == 99
      @$("#main-block-age-month").text("??")
    else 
      @$("#main-block-age-month").text(age_month)
    @$("#main-block-gendar").text(Www.const.gendar_jp[tmp_word.get("gendar")])
    @$("#main-block-translate").text(tmp_word.get("translate"))
    @$("#main-block-comment").text(tmp_word.get("comment"))

    # ログインユーザ自身の投稿だったら編集ボタン表示
    if Www.var.signed_in == 1 && Www.model.user_profile.get("user_id") == tmp_word.get("user_id")
      @$("#main-block-edit").css("display", "block")
      @$("#main-block-report").css("display", "none")
    else
      @$("#main-block-edit").css("display", "none")
      @$("#main-block-report").css("display", "block")


class Www.Views.Menu extends Backbone.View
  el: "#menu-block"

  events:
    # クローズアイコンを押す
    "click #menu-block-close-icon": "close_dialog"
    # このサイトについて
    "click .menu .about": "show_about"
    # 利用規約
    "click .menu .agreement": "show_agreement"
    # 問い合わせ
    "click .menu .inquiry": "show_inquiry"
  
  initialize: ->

  close_dialog: ->
    this.$el.css("display", "none")
    this.$el.children("div").css("display", "none")

  show_about: ->
    Www.mediator.trigger("about-dialog:render")

  show_agreement: ->
    Www.mediator.trigger("agreement-dialog:render_without_button")

  show_inquiry: ->
    Www.mediator.trigger("inquiry-dialog:render")


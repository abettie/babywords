class Www.Views.LoginDialog extends Backbone.View
  el: "#login-dialog"

  events:
    # クローズアイコンを押す
    "click #login-dialog-close-icon": "close_dialog"
  
  initialize: ->
    # 表示要求イベントを監視
    this.listenTo(Www.mediator, "login-dialog:render", this.render)

  close_dialog: ->
    this.$el.css("display", "none")
    this.$el.children("div").css("display", "none")

  render: ->
    if (typeof $.cookie(Www.const.agreement_key) == "undefined" || $.cookie(Www.const.agreement_key) < Www.const.agreement_version)
      # 規約に同意していない or 規約が更新された
      Www.mediator.trigger("agreement-dialog:render")
    else
      # 規約に同意済み
      location.href = Www.const.url_login

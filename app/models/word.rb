class Word < ActiveRecord::Base
  belongs_to :user
  belongs_to :user_profile, foreign_key: "user_id", primary_key: "user_id"
  has_many :reports
end

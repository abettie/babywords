class UserProfile < ActiveRecord::Base
  has_many :words
  has_one :user
end

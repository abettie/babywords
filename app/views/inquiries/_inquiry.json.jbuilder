json.extract! inquiry, :id, :user_id, :kind_id, :body, :created_at, :updated_at
json.url inquiry_url(inquiry, format: :json)
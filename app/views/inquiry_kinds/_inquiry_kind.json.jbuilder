json.extract! inquiry_kind, :id, :name, :created_at, :updated_at
json.url inquiry_kind_url(inquiry_kind, format: :json)